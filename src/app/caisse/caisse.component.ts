import { Component, OnInit } from '@angular/core';
import { CaisseService } from '../services/caisse.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ConsultationService } from '../services/consultation.service';


@Component({
  selector: 'app-caisse',
  templateUrl: './caisse.component.html',
  styleUrls: ['./caisse.component.css']
})
export class CaisseComponent implements OnInit {

  constructor(private cService:CaisseService, private CService:ConsultationService) { }
  newCaisse:any;
  listeCaisses: any;
  newType: any;
  newRecette: any;
  listeConsultations: any;
  positionedit:number;

  ngOnInit() {
    
      this.CService.getConsultations().subscribe((data)=>{this.listeConsultations=data});
      
      this.cService.getCaisse().subscribe((data)=>{
        this.listeCaisses=data;
      },
      (erreur)=>{
        console.log(erreur);
      });
  this.newCaisse= new FormGroup({
    type_caisse : new FormControl(),
    numCaisse: new FormControl(),
    montant: new FormControl,
    motifDepense: new FormControl(),
    dateDepense: new FormControl(),
    dateRecette: new FormControl(),
    typeRecette: new FormControl(),
  
   numCons : new FormControl(),
    consult: new FormGroup({numCons: new FormControl()})
      });
    
      this.newType=new FormGroup({ type_caisse: new FormControl()})
      this.newRecette=new FormGroup({ typeRecette: new FormControl()})
  }
  
  addType(){
    this.newCaisse.controls['type_caisse'].setValue(this.newType.controls['type_caisse'].value);
    console.log(this.newType.controls['type_caisse'].value);

  }

  addTyperecette(){
    this.newCaisse.controls['typeRecette'].setValue(this.newRecette.controls['typeRecette'].value);
    console.log(this.newRecette.controls['typeRecette'].value);
  
  }
  addCaisse(){
    if (this.newCaisse.controls['type_caisse'].value=='Recettes'){
      this.cService.addRecette(this.newCaisse.value).subscribe(
        Response => {
         this.listeCaisses.push(this.newCaisse.value)},
         )
         window.location.reload();
    }
    else if (this.newCaisse.controls['type_caisse'].value=='Depenses'){
      this.cService.addDepense(this.newCaisse.value).subscribe(
        Response => {
        this.listeCaisses.push(this.newCaisse.value)},
        )
        window.location.reload();
    }
    else if (this.newCaisse.controls['type_caisse'].value=='Impayes'){
      this.cService.addImpaye(this.newCaisse.value).subscribe(
        Response => {
        this.listeCaisses.push(this.newCaisse.value)},
         )
         
         window.location.reload(); 
    }
  }
    deleteCaisse(id:number, position:number){
      this.cService.delCaisse(id).subscribe(
        Response => this.listeCaisses.splice(position,1)
      )
    }
    updateCaisse (Caisse:any, position:number){
      // console.log(this.newActe);
      this.newCaisse.controls['numCaisse'].setValue(Caisse.numCaisse);
      this.newCaisse.controls['type_caisse'].setValue(Caisse.type_caisse);
      this.newCaisse.controls['montant'].setValue(Caisse.montant);
      this.newCaisse.controls['motifDepense'].setValue(Caisse.motifDepense);
      this.newCaisse.controls['dateDepense'].setValue(Caisse.dateDepense);
      this.newCaisse.controls['dateRecette'].setValue(Caisse.dateRecette);
      this.newCaisse.controls['typeRecette'].setValue(Caisse.typeRecette);
      this.newCaisse.controls['consult'].controls['numCons'].setValue(Caisse.consult.numCons);
        
      this.positionedit=position;
    
    }
  }



