import { Injectable } from '@angular/core';

import {HttpClient, HttpResponse} from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class ConsultationService {

  constructor(private http:HttpClient) { }

  getConsultations(){
    return this.http.get('http://localhost:8080/Consultation');
  }

  addConsultation(consultation:any){
    return this.http.post('http://localhost:8080/addConsultation',consultation);
  }

  delConsultation(id:any){
    return this.http.delete('http://localhost:8080/deleteConsultation/'+id)
  }

  getHistoriqueConsultations(num:any) {
    return this.http.get('http://localhost:8080/historiqueConsultation/'+num);
  }
}
