import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PrescriptionService {

  constructor(private http:HttpClient) { }

  getPrescriptions(){
    return this.http.get('http://localhost:8080/Prescription');
  }

  addPrescription(prescription:any){
    return this.http.post('http://localhost:8080/addPrescription',prescription);
  }

  delPrescription(id:any){
    return this.http.delete('http://localhost:8080/deletePrescription/'+id)
  }

  findPrescription(id:any){
    return this.http.get('http://localhost:8080/Ordonnance/'+id)
  }
}
