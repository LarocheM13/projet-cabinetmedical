import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MedicamentService {

  constructor(private http:HttpClient) { }

  getMedicaments(){
    return this.http.get('http://localhost:8080/Medicament');
  }

  addMedicament(medicament:any){
    return this.http.post('http://localhost:8080/addMedicament',medicament);
  }

  delMedicament(id:any){
    return this.http.delete('http://localhost:8080/deleteMedicament/'+id)
  }
}
