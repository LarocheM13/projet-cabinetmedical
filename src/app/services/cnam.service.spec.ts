import { TestBed } from '@angular/core/testing';

import { CNAMService } from './cnam.service';

describe('CNAMService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CNAMService = TestBed.get(CNAMService);
    expect(service).toBeTruthy();
  });
});
