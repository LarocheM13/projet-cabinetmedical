import { Injectable } from '@angular/core';
import{ HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CNAMService {

  constructor(private http:HttpClient) { }
  getCNAM(){
    return this.http.get('http://localhost:8080/CNAM');
  }


addCNAM(CNAM:any){
  return this.http.post('http://localhost:8080/saveCNAM',CNAM);
}}

