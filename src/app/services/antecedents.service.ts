import { Injectable } from '@angular/core';
import{ HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AntecedentsService {

  constructor(private http:HttpClient) { }
  
  getAntecedent(){
    return this.http.get('http://localhost:8080/Antecedents');
  }
  delAntecedent(id:any){
    return this.http.delete('http://localhost:8080/deleteAntecedents/'+id)
  }
addAntecedent(Antecedent:any){
  return this.http.post('http://localhost:8080/saveAntecedents',Antecedent);
}}