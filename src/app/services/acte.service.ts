import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActeService {

  constructor(private http:HttpClient) { }

  getActes(){
    return this.http.get('http://localhost:8080/Acte');
  }

  addActe(acte:any){
    return this.http.post('http://localhost:8080/addActe',acte);
  }

  delActe(id:any){
    return this.http.delete('http://localhost:8080/deleteActe/'+id)
  }
}
