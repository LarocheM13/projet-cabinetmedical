import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExamenService {

  constructor(private http:HttpClient) { }


  getImage(){
    return this.http.get('http://localhost:8080/check/img');
  }

  getExamen(){
    return this.http.get('http://localhost:8080/Examen');
  }


  addExamClinique(examen: any){

    return this.http.post('http://localhost:8080/addExamenClinique', examen);
  }

  addExamComplementaire(examen: any){
    return this.http.post('http://localhost:8080/addExamenComplementaire', examen);
  }


  delExamen(id:any){
    return this.http.delete('http://localhost:8080/deleteExamen/'+id)
  }


  // thumbnailFetchUrl : string = "https://south/generateThumbnail?width=100&height=100&smartCropp";
  // getBlobThumbnail(img:any): Observable<Blob> {  
  //   const headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'Accept': 'application/json'
  //   });
  //   return this.http.post<Blob>(this.thumbnailFetchUrl,
  //     {
  //       "url": img
  //     }, {headers: headers, responseType: 'blob' as 'json' });
  // }

  }

  

