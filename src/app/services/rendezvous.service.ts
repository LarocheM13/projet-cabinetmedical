import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RendezvousService {

  constructor(private http:HttpClient) { }


  getRendezVous(){
    return this.http.get('http://localhost:8080/RendezVous');
  }
  addRendezvous(RendezVous:any){
    return this.http.post('http://localhost:8080/addRendezVous',RendezVous);
  }
  delRendezvous(id:any){
    return this.http.delete('http://localhost:8080/deleteRendezVous/'+id)
  }
}
