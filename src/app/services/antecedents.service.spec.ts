import { TestBed } from '@angular/core/testing';

import { AntecedentsService } from './antecedents.service';

describe('AntecedentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AntecedentsService = TestBed.get(AntecedentsService);
    expect(service).toBeTruthy();
  });
});
