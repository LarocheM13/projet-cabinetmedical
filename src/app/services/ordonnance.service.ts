import { Injectable } from '@angular/core';
import{ HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class OrdonnanceService {

  constructor(private http:HttpClient) { }

  getOrdonnance(){
    return this.http.get('http://localhost:8080/Ordonnance');
  }
  
addOrdonnance(Ordonnance:any){
  return this.http.post('http://localhost:8080/addOrdonnance',Ordonnance);
}


}

