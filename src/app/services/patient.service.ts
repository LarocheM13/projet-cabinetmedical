import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private http:HttpClient) { }

  getPatients(){
    return this.http.get('http://localhost:8080/Patient');
  }

  addPatient(patient:any){
    return this.http.post('http://localhost:8080/savePatient',patient);
  }

  delPatient(id:any){
    return this.http.delete('http://localhost:8080/delPatient/'+id)
  }
  
  updatePatient(patient:any, id:number){
    return this.http.put('http://localhost:8080/edit_Patient/'+id,patient)
  }

getHistorique(numPatient:number){
  return this.http.get('http://localhost:8080/historiqueConsultation/'+numPatient);
}
getHistoriqueOrdo(numPatient:number){
  return this.http.get('http://localhost:8080/historiqueOrdonnance/'+numPatient);
}

getHistoriqueAnt(numPatient:number){
  return this.http.get('http://localhost:8080/historiqueAntecedents/'+numPatient);
}

getHistoriqueExam(numPatient:number){
  return this.http.get('http://localhost:8080/historiqueExamen/'+numPatient);
}


} 
