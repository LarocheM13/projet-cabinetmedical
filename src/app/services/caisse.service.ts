import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CaisseService {

  constructor(private http:HttpClient) { }

  getCaisse(){
    return this.http.get('http://localhost:8080/Caisse');
  }


  addRecette(recette:any){

    return this.http.post('http://localhost:8080/addRecettes', recette);
  }
  addDepense(depense:any){

    return this.http.post('http://localhost:8080/addDepenses', depense);
  }
  addImpaye(impaye:any){
    return this.http.post('http://localhost:8080/addImpayes', impaye);
  }


  delCaisse(id:any){
    return this.http.delete('http://localhost:8080/deleteCaisse/'+id)
  }
}



