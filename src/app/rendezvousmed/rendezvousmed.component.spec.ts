import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RendezvousmedComponent } from './rendezvousmed.component';

describe('RendezvousmedComponent', () => {
  let component: RendezvousmedComponent;
  let fixture: ComponentFixture<RendezvousmedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RendezvousmedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RendezvousmedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
