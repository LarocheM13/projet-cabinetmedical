import { Component, OnInit } from '@angular/core';
import {RendezvousService} from '../services/rendezvous.service';
import { FormGroup, FormControl } from '@angular/forms';
import {PatientService} from '../services/patient.service';

@Component({
  selector: 'app-rendezvousmed',
  templateUrl: './rendezvousmed.component.html',
  styleUrls: ['./rendezvousmed.component.css']
})
export class RendezvousmedComponent implements OnInit {

  constructor(private rService:RendezvousService, private pService:PatientService) { }

  listePatients:any;
  listeRendezVous:any;
  newRendezvous:any;
  positionedit:number;


  ngOnInit() {
  

this.pService.getPatients().subscribe((data)=>{this.listePatients=data});
this.rService.getRendezVous().subscribe((data2)=>{ this.listeRendezVous=data2;
  
},
(erreur)=>{
  console.log(erreur);
});

this.newRendezvous= new FormGroup({
numRdv: new FormControl(),
presenceRdv: new FormControl(),
dateRdv : new FormControl(),
p: new FormGroup({numPatient: new FormControl()}),
});

}

logForm(){
console.log(this.newRendezvous.value)
this.rService.addRendezvous(this.newRendezvous.value).subscribe(
  Response => {
    // this.listeActes.splice(this.positionedit,1),
  this.listeRendezVous.push(this.newRendezvous.value)},
   )
}

delRendezvous(id:number, position:number){
this.rService.delRendezvous(id).subscribe(
  Response => {this.listeRendezVous.splice(position,1)},
)
}

updateRendezvous(Rendezvous:any, position:number){
// console.log(this.newActe);
this.newRendezvous.controls['numRdv'].setValue(Rendezvous.numRdv);

this.newRendezvous.controls['presenceRdv'].setValue(Rendezvous.presenceRdv);

this.newRendezvous.controls['dateRdv'].setValue(Rendezvous.dateRdv);

this.newRendezvous.controls['p'].controls['numPatient'].setValue(Rendezvous.p.numPatient);

this.positionedit=position;
}


}