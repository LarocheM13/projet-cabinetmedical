import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormajoutsecComponent } from './formajoutsec.component';

describe('FormajoutsecComponent', () => {
  let component: FormajoutsecComponent;
  let fixture: ComponentFixture<FormajoutsecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormajoutsecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormajoutsecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
