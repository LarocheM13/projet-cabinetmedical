import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {PatientService} from "../services/patient.service"
import {CNAMService} from "../services/cnam.service"
import {ConsultationService} from "../services/consultation.service"
import {OrdonnanceService} from "../services/ordonnance.service"
import {PrescriptionService} from "../services/prescription.service"
import {MedicamentService} from "../services/medicament.service"
import {AntecedentsService} from "../services/antecedents.service"
import {RendezvousService} from '../services/rendezvous.service';
import { CaisseService } from '../services/caisse.service';

@Component({
  selector: 'app-formajoutsec',
  templateUrl: './formajoutsec.component.html',
  styleUrls: ['./formajoutsec.component.css']
})
export class FormajoutsecComponent implements OnInit {

  constructor(private CaisseService:CaisseService,private rService:RendezvousService, private aService:AntecedentsService, private mService:MedicamentService, private prService:PrescriptionService,private oService:OrdonnanceService, private cService:ConsultationService, private pService:PatientService, private CService:CNAMService) {}
  listeConsultations: any;
  newCaisse:any;
  listeCaisses: any;
  newType: any;
  newRecette: any;
  listecnam:any;
  newCNAM:any;
  listeRendezVous:any;
  newRendezvous:any;
  listeMedicaments:any;
  listeOrdonnance:any;
  listeHistoriqueConsultations:any;
  listeHistoriqueOrdonnances:any;
  listeHistoriqueAntecedents:any;
  listePrescription:any;
  listePatients:any;
  
  newPatient:any;
  positionedit:number;
   

  ngOnInit() {
    

    this.pService.getPatients().subscribe((data)=>{
      this.listePatients=data});
      this.CService.getCNAM().subscribe((data2)=>{ this.listecnam=data2});
      this.rService.getRendezVous().subscribe((data2)=>{ this.listeRendezVous=data2});
      this.cService.getConsultations().subscribe((data)=>{this.listeConsultations=data});
      
    
    (erreur)=>{
      console.log(erreur);
    };
    
    this.newPatient= new FormGroup({
    numPatient: new FormControl(),
    cin: new FormControl(),
    nomPatient: new FormControl(),
    sexePatient: new FormControl(),
    adressePatient: new FormControl(),
    telPatient: new FormControl(),
    metierPatient: new FormControl(),
    dateNaissancePatient: new FormControl(),
    
    p: new FormGroup({numSecuPatient: new FormControl()}),
    });

    this.newRendezvous= new FormGroup({
      numRdv: new FormControl(),
      presenceRdv: new FormControl(),
      dateRdv : new FormControl(),
      p: new FormGroup({numPatient: new FormControl()}),
  });

  this.CService.getCNAM().subscribe((data)=>{this.listecnam=data}), 
(erreur)=>{
  console.log(erreur);
};

this.newCNAM= new FormGroup({
  numSecuPatient: new FormControl(),
  dateValidite: new FormControl(),
  typeCNAM : new FormControl(),
  taux : new FormControl(),
});
this.CaisseService.getCaisse().subscribe((data)=>{
  this.listeCaisses=data;
},
(erreur)=>{
  console.log(erreur);
});
this.newCaisse= new FormGroup({
type_caisse : new FormControl(),
numCaisse: new FormControl(),
montant: new FormControl,
motifDepense: new FormControl(),
dateDepense: new FormControl(),
dateRecette: new FormControl(),
typeRecette: new FormControl(),

numCons : new FormControl(),
consult: new FormGroup({numCons: new FormControl()})
});

this.newType=new FormGroup({ type_caisse: new FormControl()})
this.newRecette=new FormGroup({ typeRecette: new FormControl()})

}
    
  logForm()
  {
    // console.log(this.newPatient.value)
    this.pService.addPatient(this.newPatient.value).subscribe(
      Response => {this.listePatients.splice(this.positionedit,1),
      this.listePatients.push(this.newPatient.value)},
       )
      //  window.location.reload();

       console.log(this.newRendezvous.value)
    this.rService.addRendezvous(this.newRendezvous.value).subscribe(
      Response => {
        // this.listeActes.splice(this.positionedit,1),
      this.listeRendezVous.push(this.newRendezvous.value)},
       )
       console.log(this.newCNAM.value)
       this.CService.addCNAM(this.newCNAM.value).subscribe(
       Response => {
       // this.listeActes.splice(this.positionedit,1),
       this.listecnam.push(this.newCNAM.value)},
       )
      //  window.location.reload();

}
  
  


  deletePatient(id:number, position:number){
    this.pService.delPatient(id).subscribe(
      Response => this.listePatients.splice(position,1)
    )
  }

  updatePatient(patient:any, position:number){
    this.newPatient.controls['numPatient'].setValue(patient.numPatient);

    this.newPatient.controls['cin'].setValue(patient.cin);
    
    this.newPatient.controls['nomPatient'].setValue(patient.nomPatient);
    
    this.newPatient.controls['sexePatient'].setValue(patient.sexePatient);
    
    this.newPatient.controls['adressePatient'].setValue(patient.adressePatient);
    
    this.newPatient.controls['telPatient'].setValue(patient.telPatient);
    
    this.newPatient.controls['metierPatient'].setValue(patient.metierPatient);

    this.newPatient.controls['dateNaissancePatient'].setValue(patient.dateNaissancePatient);
    
    this.newPatient.controls['p'].controls['numSecuPatient'].setValue(patient.p.numSecuPatient);

    this.positionedit=position;
  }

  updateRendezvous(Rendezvous:any, position:number){
    // console.log(this.newActe);
    this.newRendezvous.controls['numRdv'].setValue(Rendezvous.numRdv);

    this.newRendezvous.controls['presenceRdv'].setValue(Rendezvous.presenceRdv);
    
    this.newRendezvous.controls['dateRdv'].setValue(Rendezvous.dateRdv);

    this.newRendezvous.controls['p'].controls['numPatient'].setValue(Rendezvous.p.numPatient);

    this.positionedit=position;
  }
  updateCNAM (CNAM:any, position:number){
    // console.log(this.newActe);
    this.newCNAM.controls['numSecuPatient'].setValue(CNAM.numSecuPatient);
  
    this.newCNAM.controls['dateValidite'].setValue(CNAM.dateValidite);
    
    this.newCNAM.controls['typeCNAM'].setValue(CNAM.typeCNAM);
  
  
    this.positionedit=position;
    
  }


  historique(numPatient:number){
    this.pService.getHistorique(numPatient).subscribe((data) => {this.listeHistoriqueConsultations=data})
  }

  historiqueordo(numPatient:number){
    this.pService.getHistoriqueOrdo(numPatient).subscribe((data) => {this.listeHistoriqueOrdonnances=data})
}

  historiqueant(numPatient:number){
  this.pService.getHistoriqueAnt(numPatient).subscribe((data) => {this.listeHistoriqueAntecedents=data})
}
addType(){
  this.newCaisse.controls['type_caisse'].setValue(this.newType.controls['type_caisse'].value);
  console.log(this.newType.controls['type_caisse'].value);

}

addTyperecette(){
  this.newCaisse.controls['typeRecette'].setValue(this.newRecette.controls['typeRecette'].value);
  console.log(this.newRecette.controls['typeRecette'].value);

}
addCaisse(){
  if (this.newCaisse.controls['type_caisse'].value=='Recettes'){
    this.CaisseService.addRecette(this.newCaisse.value).subscribe(
      Response => {
       this.listeCaisses.push(this.newCaisse.value)},
       )
       window.location.reload();
  }
  else if (this.newCaisse.controls['type_caisse'].value=='Depenses'){
    this.CaisseService.addDepense(this.newCaisse.value).subscribe(
      Response => {
      this.listeCaisses.push(this.newCaisse.value)},
      )
      window.location.reload();
  }
  else if (this.newCaisse.controls['type_caisse'].value=='Impayes'){
    this.CaisseService.addImpaye(this.newCaisse.value).subscribe(
      Response => {
      this.listeCaisses.push(this.newCaisse.value)},
       )
       
       window.location.reload(); 
  }
}
  deleteCaisse(id:number, position:number){
    this.CaisseService.delCaisse(id).subscribe(
      Response => this.listeCaisses.splice(position,1)
    )
  }
  updateCaisse (Caisse:any, position:number){
    // console.log(this.newActe);
    this.newCaisse.controls['numCaisse'].setValue(Caisse.numCaisse);
    this.newCaisse.controls['type_caisse'].setValue(Caisse.type_caisse);
    this.newCaisse.controls['montant'].setValue(Caisse.montant);
    this.newCaisse.controls['motifDepense'].setValue(Caisse.motifDepense);
    this.newCaisse.controls['dateDepense'].setValue(Caisse.dateDepense);
    this.newCaisse.controls['dateRecette'].setValue(Caisse.dateRecette);
    this.newCaisse.controls['typeRecette'].setValue(Caisse.typeRecette);
    this.newCaisse.controls['consult'].controls['numCons'].setValue(Caisse.consult.numCons);
      
    this.positionedit=position;
  
  }
}

  