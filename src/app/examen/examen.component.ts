import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl } from '@angular/forms';
import {ExamenService} from "../services/examen.service"
import { ConsultationService } from '../services/consultation.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-examen',
  templateUrl: './examen.component.html',
  styleUrls: ['./examen.component.css']
  
})
export class ExamenComponent implements OnInit {

  constructor(private eService:ExamenService, private cService:ConsultationService, private httpClient: HttpClient) { }
  title = 'ImageUploaderFrontEnd';
  imageBlobUrl: any;
  public selectedFile : any;
  public event1: any;
  imgURL: any;
  receivedImageData: any;
  base64Data: any;
  convertedImage: any;
  listeConsultations:any;
  newExam:any;
  listeExamens:any;
  newType:any;
  positionedit:number;
  listeImage:any;

  public  onFileChanged(event:any) {
    // console.log(event);
    this.selectedFile = event.target.files[0];

    // Below part is used to display the selected image
    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (event2) => {
      this.imgURL = reader.result;
  };

 }


 // This part is for uploading
 onUpload() {


  const uploadData = new FormData();
  uploadData.append('myFile', this.selectedFile, this.selectedFile.name);


  this.httpClient.post('http://localhost:8080/check/upload', uploadData)
  .subscribe(
               res => {
                //  console.log(res);
                       this.receivedImageData = res;
                       this.newExam.controls['image'].controls['id'].setValue(this.receivedImageData.id);
                       this.base64Data = this.receivedImageData.pic;
                       this.convertedImage = 'data:image/jpeg;base64,' + this.base64Data;
                       },
               err => console.log('Error Occured duringng saving: ' + err)
            );
            
              
          

 }

 


createImageFromBlob(image: any) {
  // console.log(this.listeExamens);
  let reader = new FileReader();
  reader.addEventListener("load", () => {
    this.imageBlobUrl = reader.result;
  }, false);
if (image) {
  this.imageBlobUrl= 'data:image/jpeg;base64,'+image;
  return this.imageBlobUrl;
  }
}



  ngOnInit() {

    this.cService.getConsultations().subscribe((data)=>{this.listeConsultations=data}
    ,
    (erreur)=>{
      console.log(erreur);
    });
    
      this.eService.getImage().subscribe((images)=>{this.listeImage=images;});

      this.eService.getExamen().subscribe((data)=>{
          this.listeExamens=data;});


this.newExam= new FormGroup({
  numExamen: new FormControl(),
 
    type : new FormControl(),
    consult: new FormGroup({numCons: new FormControl()}),
    taillePatient : new FormControl(),
    temperaturePatient : new FormControl(),
    tensionPatient : new FormControl(),
    poidsPatient : new FormControl(),
    resaExamenComplementaire : new FormControl(),
    photExamenComplementaire : new FormControl(),
    image : new FormGroup({id: new FormControl()}),
    description: new FormControl(),
    });
  
    this.newType=new FormGroup({ type: new FormControl()})

}

addType(){
  this.newExam.controls['type'].setValue(this.newType.controls['type'].value);
  // console.log(this.newType.controls['type'].value);
}

addExam(){
if (this.newExam.controls['type'].value=='Clinique'){
  this.eService.addExamClinique(this.newExam.value).subscribe(
    Response => {
    this.listeExamens.push(this.newExam.value)},
     )
     window.location.reload(); 
}

else if (this.newExam.controls['type'].value=='Complementaire'){

  this.eService.addExamComplementaire(this.newExam.value).subscribe(
    Response => {
    this.listeExamens.push(this.newExam.value)},
     )
     window.location.reload();  
}
}
updateExamen (Examen:any, position:number){

  this.newExam.controls['numExamen'].setValue(Examen.numExamen);
  this.newExam.controls['description'].setValue(Examen.description);
  this.newExam.controls['type'].setValue(Examen.type);
  this.newExam.controls['consult'].controls['numCons'].setValue(Examen.consult.numCons);

  if (Examen.type=='Clinique'){
  this.newExam.controls['taillePatient'].setValue(Examen.taillePatient);
  this.newExam.controls['tensionPatient'].setValue(Examen.tensionPatient);
  this.newExam.controls['poidPatient'].setValue(Examen.poidsPatient);

}
else if (Examen.type=='Complementaire'){
  this.newExam.controls['resaExamenComplementaire'].setValue(Examen.resaExamenComplementaire);
  this.newExam.controls['image'].controls['id'].setValue(Examen.image.id);}
  this.positionedit=position;

}
deleteExamen(id:number, position:number){
  this.eService.delExamen(id).subscribe(
    Response => this.listeExamens.splice(position,1)
  )
}


}