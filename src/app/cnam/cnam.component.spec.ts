import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CNAMComponent } from './cnam.component';

describe('CNAMComponent', () => {
  let component: CNAMComponent;
  let fixture: ComponentFixture<CNAMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CNAMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CNAMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
