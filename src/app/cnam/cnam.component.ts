import { Component, OnInit } from '@angular/core';
import {CNAMService} from '../services/cnam.service';
import { FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'app-cnam',
  templateUrl: './cnam.component.html',
  styleUrls: ['./cnam.component.css']
})
export class CNAMComponent implements OnInit {

  constructor(private CService:CNAMService) { }
  listecnam:any;
  newCNAM:any;
  positionedit:number;

  ngOnInit() {
    this.CService.getCNAM().subscribe((data)=>{this.listecnam=data}), 
(erreur)=>{
  console.log(erreur);
};

this.newCNAM= new FormGroup({
  numSecuPatient: new FormControl(),
  dateValidite: new FormControl(),
  typeCNAM : new FormControl(),
  taux : new FormControl(),
});
}

logForm(){
console.log(this.newCNAM.value)
this.CService.addCNAM(this.newCNAM.value).subscribe(
Response => {
// this.listeActes.splice(this.positionedit,1),
this.listecnam.push(this.newCNAM.value)},
)
window.location.reload();

}

updateCNAM (CNAM:any, position:number){
  // console.log(this.newActe);
  this.newCNAM.controls['numSecuPatient'].setValue(CNAM.numSecuPatient);

  this.newCNAM.controls['dateValidite'].setValue(CNAM.dateValidite);
  
  this.newCNAM.controls['typeCNAM'].setValue(CNAM.typeCNAM);


  this.positionedit=position;
  
}
}
