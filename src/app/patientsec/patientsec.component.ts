import { Component, OnInit } from '@angular/core';

import {FormControl, FormGroup} from '@angular/forms';
import {PatientService} from "../services/patient.service"
import {CNAMService} from "../services/cnam.service"
import {ConsultationService} from "../services/consultation.service"

@Component({
  selector: 'app-patientsec',
  templateUrl: './patientsec.component.html',
  styleUrls: ['./patientsec.component.css']
})
export class PatientsecComponent implements OnInit {

  constructor(private cService:ConsultationService, private pService:PatientService, private CService:CNAMService) { }
  
  
  listePatients:any;
  listecnam:any;
  newPatient:any;
  positionedit:number;
  ngOnInit() { this.pService.getPatients().subscribe((data)=>{
    this.listePatients=data});
    this.CService.getCNAM().subscribe((data2)=>{ this.listecnam=data2});
  
  (erreur)=>{
    console.log(erreur);
  };
  this.newPatient= new FormGroup({
  numPatient: new FormControl(),
  cin: new FormControl(),
  nomPatient: new FormControl(),
  sexePatient: new FormControl(),
  adressePatient: new FormControl(),
  telPatient: new FormControl(),
  metierPatient: new FormControl(),
  dateNaissancePatient: new FormControl(),
  p: new FormGroup({numSecuPatient: new FormControl()}),
  });

}

logForm(){
  // console.log(this.newPatient.value)
  this.pService.addPatient(this.newPatient.value).subscribe(
    Response => {this.listePatients.splice(this.positionedit,1),
    this.listePatients.push(this.newPatient.value)},
     )
     window.location.reload();
}

deletePatient(id:number, position:number){
  this.pService.delPatient(id).subscribe(
    Response => this.listePatients.splice(position,1)
  )
}

updatePatient(patient:any, position:number){
  this.newPatient.controls['numPatient'].setValue(patient.numPatient);

  this.newPatient.controls['cin'].setValue(patient.cin);
  
  this.newPatient.controls['nomPatient'].setValue(patient.nomPatient);
  
  this.newPatient.controls['sexePatient'].setValue(patient.sexePatient);
  
  this.newPatient.controls['adressePatient'].setValue(patient.adressePatient);
  
  this.newPatient.controls['telPatient'].setValue(patient.telPatient);
  
  this.newPatient.controls['metierPatient'].setValue(patient.metierPatient);

  this.newPatient.controls['dateNaissancePatient'].setValue(patient.dateNaissancePatient);
  
  this.newPatient.controls['p'].controls['numSecuPatient'].setValue(patient.p.numSecuPatient);

  this.positionedit=position;
}

    
  }


