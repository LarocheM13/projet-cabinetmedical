import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientsecComponent } from './patientsec.component';

describe('PatientsecComponent', () => {
  let component: PatientsecComponent;
  let fixture: ComponentFixture<PatientsecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientsecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientsecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
