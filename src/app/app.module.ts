import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { PatientComponent } from './patient/patient.component';
import { PatientService } from './services/patient.service';
import { HttpClientModule } from '@angular/common/http';
import { ExamenComponent } from './examen/examen.component';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { ActeComponent } from './acte/acte.component';
import { ConsultationComponent } from './consultation/consultation.component';
import { RendezvousComponent } from './rendezvous/rendezvous.component';
import { AntecedentsComponent } from './antecedents/antecedents.component';
import { CNAMComponent } from './cnam/cnam.component';
import { OrdonnanceComponent } from './ordonnance/ordonnance.component';
import { MedicamentComponent } from './medicament/medicament.component';
import { PrescriptionComponent } from './prescription/prescription.component';
import { FormulaireajoutComponent } from './formulaireajout/formulaireajout.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatStepperModule} from '@angular/material';
import {MatExpansionModule} from '@angular/material';
import { CaisseComponent } from './caisse/caisse.component';
import { FormajoutsecComponent } from './formajoutsec/formajoutsec.component';
import { PatientsecComponent } from './patientsec/patientsec.component';
import { RendezvousmedComponent } from './rendezvousmed/rendezvousmed.component';

@NgModule({
  declarations: [

    AppComponent,
    PatientComponent,
    ExamenComponent,
    ActeComponent,
    ConsultationComponent,
    RendezvousComponent,
    AntecedentsComponent,
    CNAMComponent,
    OrdonnanceComponent,
    MedicamentComponent,
    PrescriptionComponent,
    FormulaireajoutComponent,
    CaisseComponent,
    FormajoutsecComponent,
    PatientsecComponent,
    RendezvousmedComponent
  ],
  
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatStepperModule,
    MatExpansionModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    
  ],

  providers: [PatientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
