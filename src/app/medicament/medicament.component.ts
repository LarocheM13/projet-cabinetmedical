import { Component, OnInit } from '@angular/core';
import { MedicamentService } from '../services/medicament.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-medicament',
  templateUrl: './medicament.component.html',
  styleUrls: ['./medicament.component.css']
})
export class MedicamentComponent implements OnInit {

  constructor(private mService:MedicamentService) { }

  
  listeMedicaments:any;
  newMedicament:any;
  positionedit:number;


  ngOnInit() {

    this.mService.getMedicaments().subscribe((data)=>{this.listeMedicaments=data;
    },
    (erreur)=>{
      console.log(erreur);
    });
    this.newMedicament= new FormGroup({
      codeMedicament: new FormControl(),
      nomMedicament: new FormControl(),
      dosageMedicament: new FormControl(),
      presentationMedicament: new FormControl()
    });

  }

  logForm(){
    console.log(this.newMedicament.value)
    this.mService.addMedicament(this.newMedicament.value).subscribe(
      Response => {
       if (this.positionedit!=null){ this.listeMedicaments.splice(this.positionedit,1)};
      this.listeMedicaments.push(this.newMedicament.value)},
       )
  }

  deleteMedicament(id:number, position:number){
    this.mService.delMedicament(id).subscribe(
      Response => this.listeMedicaments.splice(position,1)
    )
  }

  updateMedicament(Medicament:any, position:number){
    // console.log(this.newMedicament);
    this.newMedicament.controls['nomMedicament'].setValue(Medicament.nomMedicament);

    this.newMedicament.controls['codeMedicament'].setValue(Medicament.codeMedicament);
    
    this.newMedicament.controls['dosageMedicament'].setValue(Medicament.dosageMedicament);

    this.newMedicament.controls['presentationMedicament'].setValue(Medicament.presentationMedicament);

    this.positionedit=position;
  }
  


}
