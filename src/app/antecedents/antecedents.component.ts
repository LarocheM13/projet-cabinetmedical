import { Component, OnInit } from '@angular/core';
import {AntecedentsService} from '../services/antecedents.service';
import { FormGroup, FormControl } from '@angular/forms';
import {PatientService} from '../services/patient.service';
@Component({
  selector: 'app-antecedents',
  templateUrl: './antecedents.component.html',
  styleUrls: ['./antecedents.component.css']
})
export class AntecedentsComponent implements OnInit {

  constructor(private aService:AntecedentsService,private pService:PatientService) { }
  listePatients:any;
  listeAntecedents:any;
  newAntecedent:any;
  positionedit:number;

  ngOnInit() {
    
    this.pService.getPatients().subscribe((data)=>{this.listePatients=data});
    this.aService.getAntecedent().subscribe((data2)=>{ this.listeAntecedents=data2;
      
       
    },
    (erreur)=>{
      console.log(erreur);
    });
    
    this.newAntecedent= new FormGroup({
      numAntecedent: new FormControl(),
    catAntecedent: new FormControl(),
    descendant : new FormControl(),
    p: new FormGroup({numPatient: new FormControl()}),
    });

  }


logForm(){
console.log(this.newAntecedent.value)
this.aService.addAntecedent(this.newAntecedent.value).subscribe(
  Response => {
    // this.listeActes.splice(this.positionedit,1),
  this.listeAntecedents.push(this.newAntecedent.value)},
   )
   window.location.reload();
   
}
delAntecedent(id:number, position:number){
  this.aService.delAntecedent(id).subscribe(
    Response => {this.listeAntecedents.splice(position,1)},
  )
}

updateAntecedent (Antecedent:any, position:number){
  // console.log(this.newActe);
  this.newAntecedent.controls['numAntecedent'].setValue(Antecedent.numAntecedent);

  this.newAntecedent.controls['catAntecedent'].setValue(Antecedent.catAntecedent);
  
  this.newAntecedent.controls['descendant'].setValue(Antecedent.descendant);

  this.newAntecedent.controls['p'].controls['numPatient'].setValue(Antecedent.p.numPatient);

  this.positionedit=position;
  
}
}
