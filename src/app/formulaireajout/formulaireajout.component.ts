import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {ConsultationService} from '../services/consultation.service';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { PatientService } from '../services/patient.service';
import { ActeService } from '../services/acte.service';
import {OrdonnanceService} from '../services/ordonnance.service';
import { MedicamentService } from '../services/medicament.service';
import { PrescriptionService } from '../services/prescription.service';
import {ExamenService} from "../services/examen.service"

@Component({
  selector: 'app-formulaireajout',
  templateUrl: './formulaireajout.component.html',
  styleUrls: ['./formulaireajout.component.css']
})
export class FormulaireajoutComponent implements OnInit {

  constructor(private eService:ExamenService, private prService:PrescriptionService, private oService:OrdonnanceService, private mService:MedicamentService, private aService:ActeService, private pService:PatientService, private cService:ConsultationService) { }
  listeConsultations:any;
  listePatients:any;
  newConsultation:any;
  positionedit:number;
  listeActes:any;
  newActe:any;
  listeOrdonnances:any;
  listeOrdonnances2:any;
  listeMedicaments:any;
  listePrescParOrdo:any;
  newOrdonnance:any;
  newPrescription:any;
  title = 'ImageUploaderFrontEnd';
  imageBlobUrl: any;
  public selectedFile : any;
  public event1: any;
  imgURL: any;
  receivedImageData: any;
  base64Data: any;
  convertedImage: any;
  newExam:any;
  listeExamens:any;
  newType:any;
  listeImage:any;

Presc:any;
medocs=[];


  ngOnInit() {
    this.cService.getConsultations().subscribe((data)=>{this.listeConsultations=data}
    ,
    (erreur)=>{
      console.log(erreur);
    });
    
      this.eService.getImage().subscribe((images)=>{this.listeImage=images;});

      this.eService.getExamen().subscribe((data)=>{
          this.listeExamens=data;});


this.newExam= new FormGroup({
  numExamen: new FormControl(),
 
    type : new FormControl(),
    consult: new FormGroup({numCons: new FormControl()}),
    taillePatient : new FormControl(),
    temperaturePatient : new FormControl(),
    tensionPatient : new FormControl(),
    poidsPatient : new FormControl(),
    resaExamenComplementaire : new FormControl(),
    photExamenComplementaire : new FormControl(),
    image : new FormGroup({id: new FormControl()}),
    description: new FormControl(),
    });
  
    this.newType=new FormGroup({ type: new FormControl()})


    this.newConsultation= new FormGroup({
      numCons: new FormControl(),
      dateCons: new FormControl(),
      motifCons : new FormControl(),
      diagnostique : new FormControl(),
      p: new FormGroup({numPatient: new FormControl()}),
    });
    
    this.cService.getConsultations().subscribe((data)=>{this.listeConsultations=data});
    this.aService.getActes().subscribe((data)=>{this.listeActes=data;
    this.pService.getPatients().subscribe((data2)=>{ this.listePatients=data2});
    },
    
     
(erreur)=>{
  console.log(erreur);
});
this.oService.getOrdonnance().subscribe((data)=>{this.listeOrdonnances=data});
    this.cService.getConsultations().subscribe((data2)=>{ this.listeConsultations=data2;},
    (erreur)=>{
      console.log(erreur);
    });
    this.mService.getMedicaments().subscribe((data)=>{this.listeMedicaments=data;
    },
    (erreur)=>{
      console.log(erreur);
    });
   
    this.newOrdonnance= new FormGroup({
    numOrdo: new FormControl(),
    dateOrdo: new FormControl(),
    consult: new FormGroup({numCons: new FormControl()}),
    });

    this.newActe= new FormGroup({
      numActe: new FormControl(),
      codeActe: new FormControl(),
      tarifActe: new FormControl(),
      consult: new FormGroup({numCons: new FormControl()}),
      });

      
      

this.newPrescription= new FormArray([
    // this.newPrescription= 
    new FormGroup({
      pk: new FormGroup({numOrdo:new FormControl(),
    codeMedicament: new FormControl()}),
      freqMedicament: new FormControl(),
      quantMedicament: new FormControl(),
      formatMedicament: new FormControl()
    })]);
  }

getPresc(i:any){
    this.prService.findPrescription(i).subscribe((data)=>{this.listePrescParOrdo=data})
  }

  
  logForm(){

    

    console.log(this.newOrdonnance.value)
   
    this.oService.addOrdonnance(this.newOrdonnance.value).subscribe(
      Response => {
        
      this.listeOrdonnances.push(this.newOrdonnance.value);
    },
       )
      //  window.location.reload();
      this.oService.getOrdonnance().subscribe((data)=>{this.listeOrdonnances2=data});

    console.log(this.newActe.value)
    this.aService.addActe(this.newActe.value).subscribe(
      Response => {
       if (this.positionedit!=null){ this.listeActes.splice(this.positionedit,1)};
      this.listeActes.push(this.newActe.value)},
       )
       
  console.log(this.newConsultation.value)
  this.cService.addConsultation(this.newConsultation.value).subscribe(
  Response => {
  // this.listeActes.splice(this.positionedit,1),
  this.listeConsultations.push(this.newConsultation.value)},
  )
  
  }

  deleteActe(id:number, position:number){
    this.aService.delActe(id).subscribe(
      Response => this.listeActes.splice(position,1)
    )
  }

  updateConsultation (Consultation:any, position:number){
    // console.log(this.newActe);
    this.newConsultation.controls['numCons'].setValue(Consultation.numCons);
  
    this.newConsultation.controls['dateCons'].setValue(Consultation.dateCons);
    
    this.newConsultation.controls['motifCons'].setValue(Consultation.motifCons);
    this.newConsultation.controls['diagnostique'].setValue(Consultation.diagnostique);
    this.newConsultation.controls['p'].controls['numPatient'].setValue(Consultation.p.numPatient);
    this.positionedit=position;
    
  }
updateActe(Acte:any, position:number){
    // console.log(this.newActe);
    this.newActe.controls['numActe'].setValue(Acte.numActe);

    this.newActe.controls['codeActe'].setValue(Acte.codeActe);
    
    this.newActe.controls['tarifActe'].setValue(Acte.tarifActe);

    this.newActe.controls['consult'].controls['numCons'].setValue(Acte.consult.numCons);

    this.positionedit=position;
  }
  addmed(){
    this.Presc=new FormGroup({
      pk: new FormGroup({numOrdo:new FormControl(),
    codeMedicament: new FormControl()}),
      freqMedicament: new FormControl(),
      quantMedicament: new FormControl(),
      formatMedicament: new FormControl()
    });
    (this.newPrescription as FormArray).push(this.Presc);
    this.medocs.push(1);
  }

  removemed(i:any){
    (this.newPrescription as FormArray).removeAt(i);
    this.medocs.splice(i,1);
  }

  addPrescription(){

    this.medocs.forEach((element, i) =>{

    this.newPrescription.controls[i].controls['pk'].controls['numOrdo'].setValue(this.listeOrdonnances2[this.listeOrdonnances2.length -1].numOrdo)
    this.prService.addPrescription(this.newPrescription.controls[i].value).subscribe(
      )
  })
  }
  
  updateOrdonnance (Ordonnance:any, position:number){
    // console.log(this.newActe);
    this.newOrdonnance.controls['numOrdo'].setValue(Ordonnance.numOrdo);
    this.newOrdonnance.controls['dateOrdo'].setValue(Ordonnance.dateOrdo);
   this.newOrdonnance.controls['consult'].controls['numCons'].setValue(Ordonnance.consult.numCons);
  
    this.positionedit=position;
    
  }



  addType(){
    this.newExam.controls['type'].setValue(this.newType.controls['type'].value);
    // console.log(this.newType.controls['type'].value);
  }
  addExam(){
    if (this.newExam.controls['type'].value=='Clinique'){
      this.eService.addExamClinique(this.newExam.value).subscribe(
        Response => {
        this.listeExamens.push(this.newExam.value)},
         )
         window.location.reload(); 
    }
    
    else if (this.newExam.controls['type'].value=='Complementaire'){
    
      this.eService.addExamComplementaire(this.newExam.value).subscribe(
        Response => {
        this.listeExamens.push(this.newExam.value)},
         )
         window.location.reload();  
    }
  }}
