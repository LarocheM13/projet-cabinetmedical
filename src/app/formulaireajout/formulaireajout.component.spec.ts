import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireajoutComponent } from './formulaireajout.component';

describe('FormulaireajoutComponent', () => {
  let component: FormulaireajoutComponent;
  let fixture: ComponentFixture<FormulaireajoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulaireajoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireajoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
