import { Component, OnInit } from '@angular/core';
import {OrdonnanceService} from '../services/ordonnance.service';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import {ConsultationService} from '../services/consultation.service';
import { MedicamentService } from '../services/medicament.service';
import { PrescriptionService } from '../services/prescription.service';
@Component({
  selector: 'app-ordonnance',
  templateUrl: './ordonnance.component.html',
  styleUrls: ['./ordonnance.component.css']
})
export class OrdonnanceComponent implements OnInit {

  constructor(private oService:OrdonnanceService, private cService:ConsultationService, private mService:MedicamentService, private pService:PrescriptionService ) { }
  listeOrdonnances:any;
  listeOrdonnances2:any;
  listeConsultations:any;
  listeMedicaments:any;
  listePrescParOrdo:any;
  newOrdonnance:any;
  newPrescription:any;
  positionedit:number;
Presc:any;
medocs=[];
  ngOnInit() {
    
    this.oService.getOrdonnance().subscribe((data)=>{this.listeOrdonnances=data});
    this.cService.getConsultations().subscribe((data2)=>{ this.listeConsultations=data2;},
    (erreur)=>{
      console.log(erreur);
    });
    this.mService.getMedicaments().subscribe((data)=>{this.listeMedicaments=data;
    },
    (erreur)=>{
      console.log(erreur);
    });
   
    this.newOrdonnance= new FormGroup({
    numOrdo: new FormControl(),
    dateOrdo: new FormControl(),
    consult: new FormGroup({numCons: new FormControl()}),
    });

this.newPrescription= new FormArray([
    // this.newPrescription= 
    new FormGroup({
      pk: new FormGroup({numOrdo:new FormControl(),
    codeMedicament: new FormControl()}),
      freqMedicament: new FormControl(),
      quantMedicament: new FormControl(),
      formatMedicament: new FormControl()
    })]);
  }


  getPresc(i:any){
    this.pService.findPrescription(i).subscribe((data)=>{this.listePrescParOrdo=data})
  }

  logForm(){
    console.log(this.newOrdonnance.value)
   
    this.oService.addOrdonnance(this.newOrdonnance.value).subscribe(
      Response => {
        
      this.listeOrdonnances.push(this.newOrdonnance.value);
    },
       )
      //  window.location.reload();
      this.oService.getOrdonnance().subscribe((data)=>{this.listeOrdonnances2=data});

    }
    
    addmed(){
      this.Presc=new FormGroup({
        pk: new FormGroup({numOrdo:new FormControl(),
      codeMedicament: new FormControl()}),
        freqMedicament: new FormControl(),
        quantMedicament: new FormControl(),
        formatMedicament: new FormControl()
      });
      (this.newPrescription as FormArray).push(this.Presc);
      this.medocs.push(1);
    }

    removemed(i:any){
      (this.newPrescription as FormArray).removeAt(i);
      this.medocs.splice(i,1);
    }

    addPrescription(){

      this.medocs.forEach((element, i) =>{

      this.newPrescription.controls[i].controls['pk'].controls['numOrdo'].setValue(this.listeOrdonnances2[this.listeOrdonnances2.length -1].numOrdo)
      this.pService.addPrescription(this.newPrescription.controls[i].value).subscribe(
        )
    })
    }
    
    updateOrdonnance (Ordonnance:any, position:number){
      // console.log(this.newActe);
      this.newOrdonnance.controls['numOrdo'].setValue(Ordonnance.numOrdo);
      this.newOrdonnance.controls['dateOrdo'].setValue(Ordonnance.dateOrdo);
     this.newOrdonnance.controls['consult'].controls['numCons'].setValue(Ordonnance.consult.numCons);
    
      this.positionedit=position;
      
    }
    }
