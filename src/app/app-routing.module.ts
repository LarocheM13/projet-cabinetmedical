import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PatientComponent } from './patient/patient.component';
import { ExamenComponent } from './examen/examen.component';
import { ActeComponent } from './acte/acte.component';
import { RendezvousComponent } from './rendezvous/rendezvous.component';
import { AntecedentsComponent } from './antecedents/antecedents.component';
import { CNAMComponent } from './cnam/cnam.component';
import {OrdonnanceComponent} from './ordonnance/ordonnance.component';
import {ConsultationComponent} from './consultation/consultation.component';
import { MedicamentComponent } from './medicament/medicament.component';
import { PrescriptionComponent } from './prescription/prescription.component';
import { FormulaireajoutComponent } from './formulaireajout/formulaireajout.component';
import { FormajoutsecComponent } from './formajoutsec/formajoutsec.component';
import { CaisseComponent } from './caisse/caisse.component';
import { PatientsecComponent } from './patientsec/patientsec.component';
import {RendezvousmedComponent} from './rendezvousmed/rendezvousmed.component';
const routes: Routes = [

  
  {path:'patient', component: PatientComponent},
  {path:'examen', component: ExamenComponent},
  {path:'acte', component: ActeComponent},
  {path:'rendezvous', component: RendezvousComponent},
  {path: 'antecedent', component: AntecedentsComponent},
  {path: 'cnam', component: CNAMComponent},
  {path: 'ordonnance', component: OrdonnanceComponent},
  {path: 'consultation', component: ConsultationComponent},
  {path: 'medicament', component: MedicamentComponent},
  {path:'prescription', component: PrescriptionComponent},
  {path:'formulaireajout', component:FormulaireajoutComponent},
  {path:'caisse', component: CaisseComponent},
  {path:'formajoutsec', component: FormajoutsecComponent},
  {path:'patientsec', component: PatientsecComponent},
  {path:'rendezvousmed', component: RendezvousmedComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
