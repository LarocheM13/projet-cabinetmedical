import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {PatientService} from "../services/patient.service"
import {CNAMService} from "../services/cnam.service"
import {ConsultationService} from "../services/consultation.service"
import {OrdonnanceService} from "../services/ordonnance.service"
import {PrescriptionService} from "../services/prescription.service"
import {MedicamentService} from "../services/medicament.service"
import {AntecedentsService} from "../services/antecedents.service"
import { ExamenService } from '../services/examen.service';


@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  constructor(private eService : ExamenService, private aService:AntecedentsService, private mService:MedicamentService, private prService:PrescriptionService,private oService:OrdonnanceService, private cService:ConsultationService, private pService:PatientService, private CService:CNAMService) { }
  listeImage:any;
  imageBlobUrl: any;
  listeHistoriqueExamens:any;
  listeMedicaments:any;
  listeOrdonnance:any;
  listeHistoriqueConsultations:any;
  listeHistoriqueOrdonnances:any;
  listeHistoriqueAntecedents:any;
  
  listePrescription:any;
  listePatients:any;
  listecnam:any;
  newPatient:any;
  positionedit:number;
  ngOnInit() {

    this.pService.getPatients().subscribe((data)=>{
      this.listePatients=data});
      this.CService.getCNAM().subscribe((data2)=>{ this.listecnam=data2});
      this.eService.getImage().subscribe((images)=>{this.listeImage=images;});
    (erreur)=>{
      console.log(erreur);
    };
    this.newPatient= new FormGroup({
    numPatient: new FormControl(),
    cin: new FormControl(),
    nomPatient: new FormControl(),
    sexePatient: new FormControl(),
    adressePatient: new FormControl(),
    telPatient: new FormControl(),
    metierPatient: new FormControl(),
    dateNaissancePatient: new FormControl(),
    p: new FormGroup({numSecuPatient: new FormControl()}),
    });

  }

  

createImageFromBlob(image: any) {
  // console.log(this.listeExamens);
  let reader = new FileReader();
  reader.addEventListener("load", () => {
    this.imageBlobUrl = reader.result;
  }, false);
if (image) {
  this.imageBlobUrl= 'data:image/jpeg;base64,'+image;
  return this.imageBlobUrl;
  }
}


  logForm(){
    // console.log(this.newPatient.value)
    this.pService.addPatient(this.newPatient.value).subscribe(
      Response => {this.listePatients.splice(this.positionedit,1),
      this.listePatients.push(this.newPatient.value)},
       )
       window.location.reload();
  }

  deletePatient(id:number, position:number){
    this.pService.delPatient(id).subscribe(
      Response => this.listePatients.splice(position,1)
    )
  }

  updatePatient(patient:any, position:number){
    this.newPatient.controls['numPatient'].setValue(patient.numPatient);

    this.newPatient.controls['cin'].setValue(patient.cin);
    
    this.newPatient.controls['nomPatient'].setValue(patient.nomPatient);
    
    this.newPatient.controls['sexePatient'].setValue(patient.sexePatient);
    
    this.newPatient.controls['adressePatient'].setValue(patient.adressePatient);
    
    this.newPatient.controls['telPatient'].setValue(patient.telPatient);
    
    this.newPatient.controls['metierPatient'].setValue(patient.metierPatient);

    this.newPatient.controls['dateNaissancePatient'].setValue(patient.dateNaissancePatient);
    
    this.newPatient.controls['p'].controls['numSecuPatient'].setValue(patient.p.numSecuPatient);

    this.positionedit=position;
  }


  historique(numPatient:number){
    this.pService.getHistorique(numPatient).subscribe((data) => {this.listeHistoriqueConsultations=data})
  }

  historiqueordo(numPatient:number){
    this.pService.getHistoriqueOrdo(numPatient).subscribe((data) => {this.listeHistoriqueOrdonnances=data})
}

  historiqueant(numPatient:number){
  this.pService.getHistoriqueAnt(numPatient).subscribe((data) => {this.listeHistoriqueAntecedents=data})
  console.log(this.listeHistoriqueAntecedents)
}


historiqueexam(numPatient:number){
  this.pService.getHistoriqueExam(numPatient).subscribe((data) => {this.listeHistoriqueExamens=data})
}


}