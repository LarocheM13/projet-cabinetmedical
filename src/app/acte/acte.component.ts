import { Component, OnInit } from '@angular/core';
import { ActeService } from '../services/acte.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ConsultationService } from '../services/consultation.service';

@Component({
  selector: 'app-acte',
  templateUrl: './acte.component.html',
  styleUrls: ['./acte.component.css']
})
export class ActeComponent implements OnInit {

  constructor(private aService:ActeService, private cService:ConsultationService) { }

  listeConsultations:any;
  listeActes:any;
  newActe:any;
  positionedit:number;


  ngOnInit() {

    this.cService.getConsultations().subscribe((data)=>{this.listeConsultations=data});
    this.aService.getActes().subscribe((data)=>{this.listeActes=data;
    },
    (erreur)=>{
      console.log(erreur);
    });
    this.newActe= new FormGroup({
    numActe: new FormControl(),
    codeActe: new FormControl(),
    tarifActe: new FormControl(),
    consult: new FormGroup({numCons: new FormControl()}),
    });

  }

  logForm(){
    console.log(this.newActe.value)
    this.aService.addActe(this.newActe.value).subscribe(
      Response => {
       if (this.positionedit!=null){ this.listeActes.splice(this.positionedit,1)};
      this.listeActes.push(this.newActe.value)},
       )
  }

  deleteActe(id:number, position:number){
    this.aService.delActe(id).subscribe(
      Response => this.listeActes.splice(position,1)
    )
  }

  updateActe(Acte:any, position:number){
    // console.log(this.newActe);
    this.newActe.controls['numActe'].setValue(Acte.numActe);

    this.newActe.controls['codeActe'].setValue(Acte.codeActe);
    
    this.newActe.controls['tarifActe'].setValue(Acte.tarifActe);

    this.newActe.controls['consult'].controls['numCons'].setValue(Acte.consult.numCons);

    this.positionedit=position;
  }


}
