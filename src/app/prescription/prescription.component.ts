import { Component, OnInit } from '@angular/core';
import { PrescriptionService } from '../services/prescription.service';
import { FormGroup, FormControl } from '@angular/forms';
import { MedicamentService } from '../services/medicament.service';
import { OrdonnanceService } from '../services/ordonnance.service';

@Component({
  selector: 'app-prescription',
  templateUrl: './prescription.component.html',
  styleUrls: ['./prescription.component.css']
})
export class PrescriptionComponent implements OnInit {

  constructor(private pService:PrescriptionService, private mService:MedicamentService, private oService:OrdonnanceService ) { }

  
  listePrescriptions:any;
  listeMedicaments:any;
  listeOrdonnances:any;
  newPrescription:any;
  positionedit:number;
  
  ngOnInit(){
    this.pService.getPrescriptions().subscribe((data)=>{this.listePrescriptions=data;
    },
    (erreur)=>{
      console.log(erreur);
    });

    this.oService.getOrdonnance().subscribe((data)=>{this.listeOrdonnances=data;
    },
    (erreur)=>{
      console.log(erreur);
    });

    this.mService.getMedicaments().subscribe((data)=>{this.listeMedicaments=data;
    },
    (erreur)=>{
      console.log(erreur);
    });

    this.newPrescription= new FormGroup({
      pk: new FormGroup({numOrdo:new FormControl(),
    codeMedicament: new FormControl()}),
      freqMedicament: new FormControl(),
      quantMedicament: new FormControl(),
      formatMedicament: new FormControl()
    });

  }

  logForm(){
    console.log(this.newPrescription.value)
    this.pService.addPrescription(this.newPrescription.value).subscribe(
      Response => {
       if (this.positionedit!=null){ this.listePrescriptions.splice(this.positionedit,1)};
      this.listePrescriptions.push(this.newPrescription.value)},
       )
  }

  deletePrescription(id:number, position:number){
    this.pService.delPrescription(id).subscribe(
      Response => this.listePrescriptions.splice(position,1)
    )
  }

  updatePrescription(Prescription:any, position:number){
    // console.log(this.newPrescription);
    this.newPrescription.controls['freqMedicament'].setValue(Prescription.freqMedicament);

    this.newPrescription.controls['quantMedicament'].setValue(Prescription.quantMedicament);
    
    this.newPrescription.controls['pk'].controls['numOrdo'].setValue(Prescription.pk.numOrdo);

    this.newPrescription.controls['pk'].controls['codeMedicament'].setValue(Prescription.pk.codeMedicament);

    this.newPrescription.controls['formatMedicament'].setValue(Prescription.formatMedicament);

    this.positionedit=position;
  }

}
