import { Component, OnInit } from '@angular/core';
import {ConsultationService} from '../services/consultation.service';
import { FormGroup, FormControl } from '@angular/forms';
import { PatientService } from '../services/patient.service';

@Component({
  selector: 'app-consultation',
  templateUrl: './consultation.component.html',
  styleUrls: ['./consultation.component.css']
})
export class ConsultationComponent implements OnInit {

  constructor(private pService:PatientService, private cService:ConsultationService) { }
  listeConsultations:any;
  listePatients:any;
  newConsultation:any;
  positionedit:number;

  ngOnInit() {
    this.cService.getConsultations().subscribe((data)=>{this.listeConsultations=data});
    this.pService.getPatients().subscribe((data2)=>{ this.listePatients=data2}); 
(erreur)=>{
  console.log(erreur);
};

this.newConsultation= new FormGroup({
  numCons: new FormControl(),
  dateCons: new FormControl(),
  motifCons : new FormControl(),
  diagnostique : new FormControl(),
  p: new FormGroup({numPatient: new FormControl()}),
});
}

logForm(){
console.log(this.newConsultation.value)
this.cService.addConsultation(this.newConsultation.value).subscribe(
Response => {
// this.listeActes.splice(this.positionedit,1),
this.listeConsultations.push(this.newConsultation.value)},
)
window.location.reload();

}

updateConsultation (Consultation:any, position:number){
  // console.log(this.newActe);
  this.newConsultation.controls['numCons'].setValue(Consultation.numCons);

  this.newConsultation.controls['dateCons'].setValue(Consultation.dateCons);
  
  this.newConsultation.controls['motifCons'].setValue(Consultation.motifCons);
  this.newConsultation.controls['diagnostique'].setValue(Consultation.diagnostique);
  this.newConsultation.controls['p'].controls['numPatient'].setValue(Consultation.p.numPatient);
  this.positionedit=position;
  
}
}